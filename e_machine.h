#include "reg52.h"

//电机IO
#define GPIO_MOTOR P1
sbit K1=P3^0;
sbit K2=P3^1;
sbit K3=P3^2;
sbit K4=P3^3;

unsigned char code FFW[8]={0xf1,0xf3,0xf2,0xf6,0xf4,0xfc,0xf8,0xf9}; //反转顺序
unsigned char code FFZ[8]={0xf9,0xf8,0xfc,0xf4,0xf6,0xf2,0xf3,0xf1}; //正转顺序


int flag = 10;
void Delay(unsigned int t);
void  Motor(unsigned char Direction);
void  motor_foreward();
void  motor_reverse();
void  motor_stop();

/*******************************************************************************
* 函 数 名         : Motor
* 函数功能		   : 电机旋转函数
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
void  Motor(unsigned char Direction)
{ 
	unsigned char i;
	for(i=0;i<8;i++)
	{
		if(Direction==1){
			GPIO_MOTOR = FFW[i]&0x1f;  //取数据
		}
		if(Direction==2) {
			GPIO_MOTOR = FFZ[i]&0x1f;
		}
		Delay(1);	//调节转速	
	}
}

/*********************步进电机正转*********************/
void  motor_foreward(unsigned char index)
{ 
	unsigned char i;
	while(index --){
		for(i=0;i<8;i++)
		{
			GPIO_MOTOR = FFZ[i]&0x1f;
			Delay(1);	//调节转速	
		}		
	}
	
}

/*********************步进电机反转*********************/
void  motor_reverse(unsigned char index)
{ 
	unsigned char i;
	while(index --){
		for(i=0;i<8;i++)
		{
			GPIO_MOTOR = FFW[i]&0x1f;
			Delay(1);	//调节转速	
		}		
	}	
}

/*********************步进电机停止*********************/
void  motor_stop()
{ 
	GPIO_MOTOR = 0x00;
}



/*******************************************************************************
* 函 数 名         : Delay
* 函数功能		   : 延时
* 输    入         : t
* 输    出         : 无
*******************************************************************************/
void Delay(unsigned int t)
{                           
	unsigned int k;
	while(t--)
	{
		for(k=0; k<80; k++)
		{ }
	}
}



